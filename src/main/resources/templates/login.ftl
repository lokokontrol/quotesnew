<#include "/header.ftl">
    <div class="container">
		
      <form name="loginForm" class="form-signin" action="/login" method="post">
        <label for="inputUser" class="sr-only">Email address</label>
		<@spring.bind "loginForm.user"/>
        <input type="email" name="${spring.status.expression}" id="${spring.status.expression}" class="form-control" placeholder="Email address">
		<@spring.showErrors "<br>"/>
        <label for="inputPassword" class="sr-only">Password</label>
        <@spring.bind "loginForm.password"/>
		<input type="password" name="${spring.status.expression}" id="${spring.status.expression}" class="form-control" placeholder="Password">
		<#list spring.status.errorMessages as error> <b>${error}</b> <br> </#list>
        <br>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      </form>

    </div> <!-- /container -->
<#include "/footer.ftl">