<#include "/header.ftl">
<div class="container">
	<#include "/ui/breadcrumb.ftl">
	<div class="row">
		<form action="/quotes/${quote.id}/edit" method="post"
			class="form-horizontal" role="form">
			<div class="form-group">
				<label for="text" class="col-lg-2 control-label">Texto</label>
				<div class="col-lg-10">
					<@spring.bind "quoteForm.text"/>
					<textarea id="text" class="form-control" rows="5" name="text">${quoteForm.text}</textarea>
				</div>
			</div>
			<div class="form-group">
				<label for="autor" class="col-lg-2 control-label">Autor</label>
				<div class="col-lg-10">
					<@spring.bind "quoteForm.author"/> <input type="text"
						class="form-control" id="author" name="author"
						value="${quoteForm.author}">
				</div>
			</div>
			<div class="form-group">
				<label for="context" class="col-lg-2 control-label">Contexto</label>
				<div class="col-lg-10">
					<@spring.bind "quoteForm.context"/>
					<textarea id="context" class="form-control" rows="5" name="context">${quoteForm.context}</textarea>
				</div>
			</div>
			<div class="form-group">
				<div class="col-lg-offset-2 col-lg-10">
					<button type="submit" class="btn btn-default">Edit</button>
				</div>
			</div>
		</form>
	</div>
</div>
<#include "/footer.ftl">
