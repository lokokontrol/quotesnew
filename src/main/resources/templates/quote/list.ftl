<#include "/header.ftl">
<div class="container">
	<#include "/ui/breadcrumb.ftl">
	<div class="row">
		<div class="table-responsive">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Text</th>
						<th>Author</th>
						<th>Context</th>
					</tr>
				</thead>
				<tbody>
					<#list quotes as quote>
					<tr>
						<td><a href="/quotes/${quote.id}">${quote.text}</a></td>
						<td><a href="/quotes/${quote.id}">${quote.author}</a></td>
						<td><a href="/quotes/${quote.id}">${quote.context}</a></td>
					</tr>
					</#list>
				</tbody>
			</table>
		</div>
	</div>
</div>
<#include "/footer.ftl">
