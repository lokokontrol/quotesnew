package quotes.quote;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author maureon
 *
 */
@RestController
@RequestMapping("/api/quotes")
@Slf4j
public class QuoteRestController {


	
	@Autowired
	QuoteRepository quoteRepository;
	
	@Autowired
	Mapper mapper;
	
	@RequestMapping(value = "")
	public Iterable<Quote> findAll() {
		return quoteRepository.findAll();
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Quote view(@PathVariable("id") Long id, Model model) {
		Quote quote = quoteRepository.findOne(id);
		log.info("{}", quote);
		return quoteRepository.findOne(id);
	}
	
	/**
	 * 
	 * @param id
	 * @param quoteForm
	 * @return
	 * @see http://g00glen00b.be/validating-the-input-of-your-rest-api-with-spring/
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public Quote update(@PathVariable("id") Long id, @Validated @RequestBody QuoteForm quoteForm) {
		Quote quote = quoteRepository.findOne(id);
		if (quote == null) {
			return null;
		}else {
			Quote updatedQuote = mapper.map(quoteForm, Quote.class);
			quote.setId(id);
			return quoteRepository.save(updatedQuote);
		}
	}
	
}
