package quotes.quote;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Data
@EqualsAndHashCode(of={"id"})
@ToString(exclude={"text", "id"})
public class Quote {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String author;
	private String text;
	private String context;
	
	public Quote(String author, String text, String context) {
		super();
		this.author = author;
		this.text = text;
		this.context = context;
	}

	public Quote() {
		super();
	}
	
}
