package quotes.quote;

import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(of={"id"})
@ToString(exclude="text")
public class QuoteForm {
	
	private String id;	
	private String author;
	@NotNull
	private String text;	
	private String context;
			
}
