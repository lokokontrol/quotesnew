package quotes.quote;

import javax.validation.Valid;

import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import quotes.ui.Breadcrumb;

@Controller
@RequestMapping("/quotes")
public class QuoteController {

	Logger log = LoggerFactory.getLogger(QuoteController.class);
	
	@Autowired
	QuoteRepository quoteRepository;
	
	@Autowired
	Mapper mapper;

	@RequestMapping(value = "")
	public String getAll(Model model) {
		Iterable<Quote> quotes = quoteRepository.findAll();
		model.addAttribute("quotes", quotes);
		model.addAttribute("breadcrumb", 
				new Breadcrumb.Builder()
				 .elem("Home", "#")
				 .elem("Quotes","/quotes")
				 .active("All").build()
				);
		return "quote/list";
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String view(@PathVariable("id") Long id, Model model) {
		Quote quote = quoteRepository.findOne(id);
		model.addAttribute("quote", quote);
		model.addAttribute("breadcrumb", 
				new Breadcrumb.Builder()
				 .elem("Home", "#")
				 .elem("Quotes","/quotes")
				 .active("Detail").build()
				);
		return "quote/view";
	}
	
	@RequestMapping(value = "/{id}/edit", method = RequestMethod.GET)
	public String viewEdit(@PathVariable("id") Long id, Model model) {
		Quote quote = quoteRepository.findOne(id);
		model.addAttribute("quote", quote);
		model.addAttribute("quoteForm", mapper.map(quote, QuoteForm.class));
		model.addAttribute("breadcrumb", 
				new Breadcrumb.Builder()
				 .elem("Home", "#")
				 .elem("Quotes","/quotes")
				 .active("Edit").build()
				);
		return "quote/edit";
	}
	
	@RequestMapping(value = "/{id}/edit", method = RequestMethod.POST)
	public String edit(@ModelAttribute @Valid QuoteForm form, BindingResult bindingResult, @PathVariable("id") Long id, Model model) {
		Quote quote = quoteRepository.findOne(id);
		model.addAttribute("quote", quote);
		model.addAttribute("breadcrumb", 
				new Breadcrumb.Builder()
				 .elem("Home", "#")
				 .elem("Quotes","/quotes")
				 .active("All").build()
				);
		return "quote/view";
	}

	@RequestMapping(value = "/new", method = RequestMethod.GET)
	public String addNewView(Model model) {
		model.addAttribute("quoteForm", new QuoteForm());
		model.addAttribute("breadcrumb", 
				new Breadcrumb.Builder()
				 .elem("Home", "#")
				 .elem("Quotes","/quotes")
				 .active("New").build()
				);
		return "quote/new";
	}

	@RequestMapping(value = "/new", method = RequestMethod.POST)
	public String addNew(@ModelAttribute @Valid QuoteForm form, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return "redirect:quote/new";
		}
		Quote quote = mapper.map(form, Quote.class);
		quoteRepository.save(quote);
		return "redirect:/quotes";
	}

}